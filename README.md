# General

**Setup**

```
npm install
```

**Run**

```
npm run start
```

**Build**

```
npm run build
```

**Configring TypeScript**

https://blog.logrocket.com/how-why-a-guide-to-using-typescript-with-react-fffb76c61614

https://www.typescriptlang.org/docs/handbook/react-&-webpack.html

```
npm install --save react react-dom @types/react @types/react-dom
```

```
npm install --save-dev typescript awesome-typescript-loader source-map-loader
```