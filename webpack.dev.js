const merge = require('webpack-merge');
const common = require('./webpack.common.js');

const path = require('path');
const SRC_DIR = path.resolve(__dirname,'src');
const DIST_DIR = path.resolve(__dirname, 'dist');
const ASSET_PATH = process.env.ASSET_PATH || '';

module.exports = merge(common, {
  devtool: 'inline-source-map',
  devServer: {
    contentBase: DIST_DIR,
    compress: false,
    port: 9000,    
    clientLogLevel: "none",
    historyApiFallback: true
  },  
  module: {
    rules: [
      // sass
      {
        test: /\.scss$/,
        use: [{
            loader: "style-loader"
        }, {
            loader: "css-loader"
        }, {
            loader: "sass-loader"          
        }]
      }      
    ]
  }
});