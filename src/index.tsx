import * as React from "react";
import * as ReactDOM from "react-dom";

const json = require('./data/sample.json');

console.log(json);

import { Hello } from "./components/Hello";


ReactDOM.render(
    <Hello compiler="TypeScript" framework="React" />,
    document.getElementById("app")
);