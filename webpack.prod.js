const merge = require('webpack-merge');
const common = require('./webpack.common.js');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const ExtractStyles = new ExtractTextPlugin({filename: 'styles.css'});
const CopyWebpackPlugin = require('copy-webpack-plugin');

const path = require('path');
const SRC_DIR = path.resolve(__dirname,'src');
const DIST_DIR = path.resolve(__dirname, 'dist');
const ASSET_PATH = process.env.ASSET_PATH || '';

module.exports = merge(common, {
    plugins: [
        new UglifyJSPlugin(),
        ExtractStyles,
        new CopyWebpackPlugin([
            { from:SRC_DIR + '/images', to: DIST_DIR + '/images' }
        ])
    ],
    module: {
        rules: [
            {
                test: /\.scss$/,
                use: ExtractStyles.extract({
                    fallback: "style-loader",
                    use: [
                        {
                            loader: 'css-loader',
                            options: {
                                url: false,
                                minimize: true,
                                sourceMap: true
                            }                            
                        },
                        { loader: "sass-loader"}
                    ]                
                })    
            }
        ]
    }
});