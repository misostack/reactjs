const webpack = require('webpack');
const path = require('path');
const HTMLPlugin = require('html-webpack-plugin');

const SRC_DIR = path.resolve(__dirname,'src');
const DIST_DIR = path.resolve(__dirname, 'dist');
const ASSET_PATH = process.env.ASSET_PATH || '';

module.exports = {
  entry: {
    // vendor: [
    //   '@babel/polyfill'
    // ],
    app: ['@babel/polyfill', SRC_DIR + '/index.tsx']
  },
  output: {
    path: DIST_DIR,
    filename: '[name].bundle.js',
    publicPath: ASSET_PATH
  },
  resolve: {
    alias: {
      components:"components",
      data:"data",
      containers:"containers",
      actions:"actions"
    },    
    modules: [SRC_DIR, "node_modules"],
    extensions: ['.ts','.tsx','.js','.jsx', '.css', '.scss','.json']
  },
  module: {
    rules: [
      // js,jsx
      {
        test: /\.(js|jsx)$/,
        exclude: /(node_modules)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: [
              ['@babel/preset-env', {
                targets: {
                  "browsers": ["last 2 versions"]
                },        
                useBuiltIns: "usage",
                debug: true,
              }],
              '@babel/preset-react'
            ],            
          }
        }
      },
      // tsx
      {
        test: /\.tsx?$/,
        loader: "ts-loader",
        exclude: /node_modules/
      },
      // css
      {
        test: /\.css$/,
        include: /node_modules/,
        loader:  ['style-loader','css-loader']
      },      
      // images
      { 
        test: /\.(png|svg|jpe?g|gif)$/i,
        use: [
          {
            loader: 'file-loader',
            options: {            
              name: 'assets/images/[name].[ext]',
              outputPath: '',
              useRelativePath: false            
            }
          }
        ]
      },
      // fonts
      { test: /\.(woff|woff2|eot|ttf|otf)$/, use: ['file-loader'] },
    ]
  },
  plugins: [
    
    new HTMLPlugin({
      title: 'Webpack 4 Example',
      // Load a custom template (lodash by default see the FAQ for details)
      template: SRC_DIR + '/index.html'      
    })    
  ]
};